#!/usr/bin/env python3

import mss
import tkinter as tk
from tkinter import filedialog, messagebox, ttk
from PIL import Image,ImageTk
import numpy as np
from threading import Timer
import time
import threadly
import _thread
import socket

import serial
from serial.tools.list_ports import comports
from templates import nr_templates

def threshold(img):
    hsv = img.convert("HSV")
    mask = hsv.getchannel(0).point(lambda h: (h > 140 and h < 180)*255)
    out = hsv.getchannel(1).point(lambda s: (s > 200)*255)
    res = Image.new("L", img.size)
    res.paste(mask, None, out)
    return res

def resize_height(img, height):
    if height == img.size[1]:
        return img
    width = int(np.ceil((height / img.size[1]) * img.size[0]))
    return img.resize((width, height),Image.NEAREST)

def split_numbers(img):
    arr = np.array(img)
    prev_c = 0
    regions = []
    for c in range(arr.shape[1]):
        if np.sum(arr[:,c]) == 0:
            tmp = arr[:,prev_c:c]
            prev_c = c+1
            if tmp.shape[1] > 1:
                regions.append(tmp)
    regions.append(arr[:,prev_c:])
    return regions
    
def get_number(region):
    nrs = [200]*10
    for nr, temp in nr_templates.items():
        to_pad = region.shape[1] - temp.shape[1]
        if to_pad < 0:
            region = np.pad(region, ((0, 0),(-to_pad,0)))
        elif to_pad > 0:
            temp = np.pad(temp, ((0, 0),(to_pad, 0)))
        
        nrs[nr] = np.sum(temp - region)
    return np.argmin(nrs)

def capture_func(app):
    timestamp = time.perf_counter()
    trig = app.trig
    try:
        raw = np.array(app.sct.grab(app.tmp_coords))
    except AttributeError:
        app.sct = mss.mss()
        return
    img = Image.fromarray(raw)
    img = threshold(img)
    bbox = img.getbbox()
    if bbox is None:
        return
    img = img.crop(bbox)
    img = resize_height(img, 16)
    regions = split_numbers(img)
    nr_str = ""
    for region in regions:
        nr_str += str(get_number(region))
    timest = "{:f}".format(timestamp - app.start_ts)[:8]
    data  = "{:8s};{:4s};{:1s}\n".format(timest,nr_str,trig)
    if not app.file is None:
        app.file.write(data)
    if app.port.get() > 0:
        app.socket.sendto(data.encode("ascii"),(app.ip.get(),app.port.get()))
        
def capture_trigger(app):
    while app.start_state:
        try:
            app.trig = app.ser.read(10)
            if len(app.trig) > 0:
                app.trig = "1"
            else:
                app.trig = "0"
        except Exception:
            app.trig = "E"
            app.ser.close()
            try:
                app.ser = serial.Serial(
                    app.trigger_port.get(),
                    baudrate=19200,
                    timeout=app.intervall/1000
                )
            except Exception:
                pass


class Application():
    def __init__(self,root):
        self.root = root
        self.rect_id = -1
        self.click_state = False
        self.start_state = False
        
        # Connection and storage stuff
        self.file = None
        self.port = tk.IntVar()
        self.port.set(0)
        
        self.trigger_port = tk.StringVar()
        self.trigger_port_unmapped = tk.StringVar()
        self.trigger_port_unmapped.trace("w", self.map_port)
        self.available_ports = {}
        for port in comports():
            self.available_ports[str(port)] = port.name
        self.ser = None
        self.trig = "F"

        self.start_ts = 0
        self.scheduler = threadly.Scheduler(10)
        self.interval = 20 #ms -> 50Hz
        self.ip = tk.StringVar()
        self.ip.set("127.0.0.1")
        
        self.disable_btns = []

        self.sct = mss.mss()
        self.sct.monitors

        self.bbox_str = tk.StringVar()
        self.status_txt = tk.StringVar()
        self.btn_txt = tk.StringVar()
        self.filename = tk.StringVar()

        self.btn_txt.set("Start")
        self.status_txt.set(" ")
        self.tmp_coords = {}
        self.img_obj = 4*[None]

        frame = ttk.Frame(self.root)
        frame.pack(fill=tk.X,side=tk.TOP, padx=5, pady=5)

        frame.grid_columnconfigure(1, weight=1)
        
        frameIP = ttk.LabelFrame(frame,text="Daten streamen")
        frameIP.grid(row=0,column=0,columnspan=2,sticky=tk.W+tk.E)
        frameIP.grid_columnconfigure(1, weight=1)
        
        ## IP AREA
        ttk.Label(frameIP,text="Server IP:").grid(row=0,column=0)
        frame2 = ttk.Frame(frameIP)
        frame2.grid(row=0,column=1,sticky=tk.W+tk.E)
        
        entry = ttk.Entry(frame2,textvariable=self.ip,justify=tk.CENTER,validate=tk.ALL)
        entry.pack(side=tk.LEFT,fill=tk.X,expand=tk.TRUE)
        entry.bind("<Enter>",lambda event: self.status_txt.set("Server IP zum streamen"))
        entry.bind("<Leave>",self.leave)
        
        ## PORT AREA
        ttk.Label(frameIP,text="Server port:").grid(row=1,column=0)
        frame2 = ttk.Frame(frameIP)
        frame2.grid(row=1,column=1,sticky=tk.W+tk.E)
        
        entry = ttk.Entry(frame2,textvariable=self.port,justify=tk.CENTER)
        entry.pack(side=tk.LEFT,fill=tk.X,expand=tk.TRUE)
        entry.bind("<Enter>",lambda event: self.status_txt.set("Server Port zum streamen"))
        entry.bind("<Leave>",self.leave)
        
        self.disable_btns.append(entry)

        ## TRIGGER AREA
        ttk.Label(frame,text="Trigger port:").grid(row=1,column=0)
        frame2 = ttk.Frame(frame)
        frame2.grid(row=1,column=1,sticky=tk.W+tk.E)

        entry = ttk.Combobox(
            frame2,
            textvariable=self.trigger_port_unmapped,
            values=list(self.available_ports.keys()),
            justify=tk.CENTER
        )
        entry.pack(side=tk.LEFT,fill=tk.X,expand=tk.TRUE)
        entry.bind("<Enter>",lambda event: self.status_txt.set("Der serielle Port für den Trigger."))
        entry.bind("<Leave>",self.leave)
        
        self.disable_btns.append(entry)

        ## PATH AREA
        tk.Label(frame,text="Dateipfad:").grid(row=2,column=0)
        frame2 = ttk.Frame(frame)
        frame2.grid(row=2,column=1,sticky=tk.W+tk.E)

        entry = ttk.Entry(frame2,textvariable=self.filename,justify=tk.CENTER)
        entry.pack(side=tk.LEFT,fill=tk.X,expand=tk.TRUE)

        clickopen = ttk.Button(frame2,text = u"\u2191",command = self.click_file)
        clickopen.pack(side=tk.RIGHT)
        clickopen.bind("<Enter>",lambda event: self.status_txt.set("Datei auswählen"))
        clickopen.bind("<Leave>",self.leave)
        
        self.disable_btns.append(clickopen)
        self.disable_btns.append(entry)

        ## AREA AREA
        ttk.Label(frame,text="Überprüfungsbereich:").grid(row=3,column=0)
        frame2 = ttk.Frame(frame)
        frame2.grid(row=3,column=1,sticky=tk.W+tk.E)

        entryLabel = ttk.Entry(frame2,textvariable=self.bbox_str,state=tk.DISABLED,justify=tk.CENTER)
        entryLabel.pack(side = tk.LEFT,fill=tk.X,expand=tk.TRUE)

        button = ttk.Button(frame2,text="A",command=self.choose_area)
        button.pack(side=tk.RIGHT)
        button.bind("<Enter>",lambda event: self.status_txt.set("Klicken um einen Bereich auszuwählen."))
        button.bind("<Leave>",self.leave)
        
        self.disable_btns.append(button)
        
        ## BOTTOM AREA
        label2 = ttk.Label(self.root,textvariable=self.status_txt)
        label2.pack(side=tk.BOTTOM)
                
        self.start_btn = ttk.Button(self.root,textvariable=self.btn_txt,state=tk.DISABLED,command=self.start)
        self.start_btn.pack(fill=tk.X,side=tk.BOTTOM)
        self.start_btn.bind("<Enter>",lambda event: self.status_txt.set("Start/Stop der Messung."))
        self.start_btn.bind("<Leave>",self.leave)
        
        ## This WILL be a problem if Software gets Larger
        self.root.update()
        self.root.minsize(root.winfo_width(), root.winfo_height())
        
    def map_port(self, *args):
        self.trigger_port.set(self.available_ports[self.trigger_port_unmapped.get()])
        
    def click_file(self):
        filename = filedialog.asksaveasfilename(
            initialdir = "./",
            title = "Select file",
            filetypes = (
                ("Comma Seperated Values","*.csv"),
                ("all files","*.*")
            )
        )
        splitted = filename.rsplit(".", 1)
        if len(splitted) < 2 or len(splitted[1]) > 5:
            filename += ".csv"
        self.filename.set(filename)

    def leave(self,event):
        self.status_txt.set(" ")

    def click_cb(self,event):
        if not self.click_state:
            self.click_state = True
            self.tmp_coords["x"] = event.x
            self.tmp_coords["y"] = event.y
        else:
            self.click_state = False
            event.widget.master.destroy()

            x1 = self.tmp_coords["x"]
            y1 = self.tmp_coords["y"]

            x2 = event.x
            y2 = event.y

            if x1 > x2:
                tmp = x1
                x1 = x2
                x2 = tmp

            if y1 > y2:
                tmp = y1
                y1 = y2
                y2 = tmp

            self.tmp_coords = {"left":x1,"top":y1,"width":x2-x1,"height":y2-y1}
            self.bbox_str.set("x:{left}, y:{top}, w:{width}, h:{height}".format(**self.tmp_coords))
            self.start_btn.config(state=tk.NORMAL)

    def motion_cb(self,event):
        if self.click_state:
            if self.rect_id >= 0:
                event.widget.delete(self.rect_id)
            x1 = self.tmp_coords["x"]
            y1 = self.tmp_coords["y"]

            x2 = event.x
            y2 = event.y
            self.rect_id = event.widget.create_rectangle((x1,y1,x2,y2),outline="red")

    def choose_area(self):
        self.status_txt.set("Bereich auswählen (2 Klicks)")

        mon0 = self.sct.monitors[0]
        self.img_obj[0] = self.sct.grab(mon0)

        self.img_obj[1] = Image.frombytes('RGB', self.img_obj[0].size, self.img_obj[0].bgra, 'raw', 'BGRX')
        t = tk.Toplevel(self.root)
        t.overrideredirect(tk.TRUE)
        t.geometry("{0}x{1}+0+0".format(mon0["width"],mon0["height"]))
        t.focus_set()
        t.bind("<Escape>",lambda e : e.widget.destroy())

        canvas = tk.Canvas(t)
        canvas.pack(expand=tk.TRUE,fill=tk.BOTH)
        canvas.bind("<Button-1>",self.click_cb)
        canvas.bind("<Motion>",self.motion_cb)

        self.img_obj[2] = ImageTk.PhotoImage(self.img_obj[1])
        self.img_obj[3] = canvas.create_image((0,0),image=self.img_obj[2],state=tk.NORMAL,anchor=tk.NW)

    def start(self):
        try:
            if self.btn_txt.get() == "Start":
                self.btn_txt.set("Stop")
                self.start_state = True
                try:
                    self.file = open(self.filename.get(),"w")
                    self.file.write("Timestamp;Value in N;Trigger\n")
                except Exception:
                    if self.port.get() <= 0:
                        messagebox.showerror("Error","No valid port and no valid file given.")
                        raise Exception()
                    else:
                        messagebox.showwarning("Warning","No valid file, data will only be streamed on port {}".format(self.port.get()))
                if self.port.get() < 0:
                    messagebox.showwarning("Warning","No valid port, data will only be stored in file {}".format(self.filename.get()))
                else:
                    self.socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
                    self.socket.setsockopt(socket.SOL_SOCKET,socket.SO_BROADCAST, True)
                    self.socket.settimeout(self.interval/1000)
                if self.trigger_port.get():
                    self.ser = serial.Serial(self.trigger_port.get(),baudrate=19200,timeout=self.interval/1000)
                    _thread.start_new_thread(capture_trigger,(self,))
                for btn in self.disable_btns:
                    btn.config(state=tk.DISABLED)
                self.start_ts = time.perf_counter()
                self.scheduler.schedule(capture_func, args=(self,), delay=self.interval, recurring=True)
                #_thread.start_new_thread ( capture_func, (self,) )
            else:
                raise Exception()
        except Exception as e:
            self.start_state = False
            self.btn_txt.set("Start")
            for btn in self.disable_btns:
                btn.config(state=tk.NORMAL)
            self.scheduler.remove(capture_func)
            #self.scheduler.shutdown_now()
            time.sleep(0.2)   
            try:
                self.file.close()
            except Exception:
                pass
            finally:
                self.file = None
                if self.ser is not None and self.ser.isOpen():
                    self.ser.close()
      
if __name__ == "__main__":
    # The root window
    root = tk.Tk()
    root.title("Grab DigiMax")
    root.iconbitmap("./assets/icon.ico")
    app = Application(root)

    root.mainloop()
