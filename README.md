# DigiMax Isorack Breakout

The Isorack Software from DigiMax is a horrible Software product and not really suitable for research purposes. 

As we had/have an forcesensor from DigiMax we need the software to acquire the data from, but for example is it not possible to get a live datastream or insert a trigger into the software (for synchronizing the data afterwards)

Some workarounds could be made to have rudimentary support for those problems.

Basically the script takes screenshots of the shown values and match the numbers as templates.

With `grabData_digimax.py` one can set a rectangle on where to read the data and send it to the network via UDP where it can be processed further and/or can store it in an .csv file.
An additional trigger can be send to the software via a serial port which is also stored in the datastream.
