# -*- mode: python ; coding: utf-8 -*-

import PyInstaller.config

import platform
import os
import sys
import site

OS = platform.system()
ARCH = platform.architecture()[0]

## Standard Information
CERTNAME = "domwet"
project_name = "digimax-isorack-breakout"
BUNDLE_IDENTIFIER="de.fh-zwickau.domwet."+project_name # Used for info.plist 

main_file = './src/grabData_digimax.py'

icon = './assets/icon.ico'
#icon = None

debug = False
cli_option = debug
#cli_option = True

datas = [
    (icon, 'assets/')
]

# datas=[(icon,'assets/')]
#if using tkinter
       
hiddenimports = [
    'packaging.specifiers'
    'packaging.requirements',
    'pacakging.markers',
    'packaging.version'
]

runtime_hooks = [
    # 'build_tools/hooks/rth_multiprocessing.py'
]

tmp_folder_system = "Windows"
#tmp_folder_system = OS ## Uncomment for debugging

## Exclude binaries from build     
exclude_binaries = [
    # 'mkl','libopenblas', 'openblas'
]

block_cipher = None

###### BOILERPLATE STUFF

## LINUX Stuff
if OS == "Linux":
    datas.extend([
        (site.getsitepackages()[0] + "/conslablib/utils/tk/linux/", "conslablib/utils/tk/linux/")
    ])

## MACOS Stuff
if OS == "Darwin":
    def fail(*msg):
        RED='\033[0;31m'
        NC='\033[0m' # No Color
        print("\r🗯 {}ERROR:{}".format(RED, NC), *msg)
        sys.exit(1)

    def codesign(identity, binary):
        d = os.path.dirname(binary)
        saved_dir=None
        if d:
            # switch to directory of the binary so codesign verbose messages don't include long path
            saved_dir = os.path.abspath(os.path.curdir)
            os.chdir(d)
            binary = os.path.basename(binary)
        os.system("codesign -v -f -s '{}' '{}'".format(identity, binary))==0 or fail("Could not code sign " + binary)
        if saved_dir:
            os.chdir(saved_dir)

    def monkey_patch_pyinstaller_for_codesigning(identity):
        # Monkey-patch PyInstaller so that we app-sign all binaries *after* they are modified by PyInstaller
        # If we app-sign before that point, the signature will be invalid because PyInstaller modifies
        # @loader_path in the Mach-O loader table.
        try:
            import PyInstaller.depend.dylib
            _saved_func = PyInstaller.depend.dylib.mac_set_relative_dylib_deps
        except (ImportError, NameError, AttributeError):
            # Hmm. Likely wrong PyInstaller version.
            fail("Could not monkey-patch PyInstaller for code signing. Please ensure that you are using PyInstaller 3.4.")
        _signed = set()
        def my_func(fn, distname):
            _saved_func(fn, distname)
            if  (fn, distname) not in _signed:
                codesign(identity, fn)
                _signed.add((fn,distname)) # remember we signed it so we don't sign again
        PyInstaller.depend.dylib.mac_set_relative_dylib_deps = my_func

    monkey_patch_pyinstaller_for_codesigning(CERTNAME)

if OS == tmp_folder_system:
    PyInstaller.config.CONF["distpath"] = "tmp_dist/{}_{}/".format(OS,ARCH)
else:
    PyInstaller.config.CONF["distpath"] = "dist/"
    project_name += "_{}_{}".format(OS,ARCH)

def remove_from_list(input, keys):
    outlist = []
    for item in input:
        name, _, _ = item
        flag = 0
        for key_word in keys:
            if name.find(key_word) > -1:
                flag = 1
        if flag != 1:
            outlist.append(item)
    return outlist

## platform specific
# if OS == "Linux":
#     datas.append(('./common/linux/','./common/linux/'))
# else:
#     hiddenimports.append('tkinter.filedialog')
    
# WORKAROUND for PyInstaller Error that does not copy tcl and tk correctly
if OS == "Darwin" and sys.version_info >= (3,7):
	py_path = os.path.split(os.path.split(sys.executable)[0])[0]
	try:
		datas.append((os.path.join(py_path,"lib","tcl*"),"./tcl"))
		datas.append((os.path.join(py_path,"lib","tk*"),"./tk"))
		datas.append((os.path.join(py_path,"lib","Tk*"),"./tk"))
	except:
		try:
			datas.append((os.path.join(py_path,"Lib","tcl*"),"./tcl"))
			datas.append((os.path.join(py_path,"Lib","tk*"),"./tk"))
			datas.append((os.path.join(py_path,"Lib","Tk*"),"./tk"))
		except:
			pass

a = Analysis([main_file],
             pathex=['./src'],
             binaries=[],
             datas=datas,
             hiddenimports=hiddenimports,
             hookspath=[],
             runtime_hooks=runtime_hooks,
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
             
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

a.binaries = remove_from_list(a.binaries, exclude_binaries)

if platform.system() == tmp_folder_system:
    exe = EXE(pyz,
              a.scripts,
              [],
              exclude_binaries=True,
              name=project_name,
              debug=debug,
              bootloader_ignore_signals=False,
              strip=False,
              upx=True,
              console=cli_option,
              icon=icon if icon else None)
    coll = COLLECT(exe,
                   a.binaries,
                   a.zipfiles,
                   a.datas,
                   strip=False,
                   upx=True,
                   upx_exclude="vcruntime140.dll",
                   name=project_name)
else:
    exe = EXE(pyz,
              a.scripts,
              a.binaries,
              a.zipfiles,
              a.datas,
              [],
              name=project_name,
              debug=debug,
              bootloader_ignore_signals=False,
              strip=False,
              upx=True,
              console=cli_option,
              icon=icon if icon else None)
              
